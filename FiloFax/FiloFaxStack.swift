//
//  FiloFaxStack.swift
//  FiloFax
//
//  Created by Tobias Ostner on 4/19/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import CoreData

func createFiloFaxStack(completion: @escaping (NSPersistentContainer) -> ()) {
    let container = NSPersistentContainer(name: "FiloFax")
    container.loadPersistentStores {
        _, error in
        guard error == nil else { fatalError("Failed to load store: \(error)") }
        DispatchQueue.main.async { completion(container) }
    }
}
