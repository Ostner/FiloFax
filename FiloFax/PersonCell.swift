//
//  PersonCell.swift
//  FiloFax
//
//  Created by Tobias Ostner on 4/19/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

final class PersonCell: UITableViewCell {
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UILabel!

    func configure(for person: Person) {
        name.text = person.name
        photo.image = person.image
    }
}
