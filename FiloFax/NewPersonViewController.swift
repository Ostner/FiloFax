//
//  File.swift
//  FiloFax
//
//  Created by Tobias Ostner on 4/21/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreData

final class NewPersonViewController: UIViewController {

    var managedContext: NSManagedObjectContext!

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var imageView: UIImageView!

    var saveBtn: UIBarButtonItem!

    var name: String = "" {
        didSet {
            guard !name.isEmpty else { return }
            saveBtn.isEnabled = true
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        saveBtn = UIBarButtonItem(
          barButtonSystemItem: .save,
          target: self,
          action: #selector(save)
        )
        navigationItem.rightBarButtonItem = saveBtn
        saveBtn.isEnabled = false
        nameField.delegate = self
    }

    // MARK: Actions

    func save() {
        guard let name = nameField.text else { fatalError("not implemented yet") }
        managedContext.performChanges {
            _ = Person.insert(
              into: self.managedContext,
              name: name,
              image: self.imageView?.image)
        }
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func showImagePicker() {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary),
        let images = UIImagePickerController.availableMediaTypes(for: .photoLibrary),
        !images.isEmpty
            else { return }
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.mediaTypes = images
        picker.delegate = self
        present(picker, animated: true)
    }
}

extension NewPersonViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        name = nameField.text ?? ""
        return true
    }
}

extension NewPersonViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }

    func imagePickerController(
      _ picker: UIImagePickerController,
      didFinishPickingMediaWithInfo info: [String : Any])
    {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        else { return }
        imageView.image = image
        dismiss(animated: true)
    }
}
