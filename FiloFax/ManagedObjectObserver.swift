//
//  ManagedObjectObserver.swift
//  FiloFax
//
//  Created by Tobias Ostner on 4/23/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import CoreData

final class ManagedObjectObserver {

    enum ChangeType {
        case delete
        case update
    }

    init?(object: NSManagedObject, handler: @escaping (ChangeType) -> ()) {
        guard let moc = object.managedObjectContext else { return nil }
        token = moc.addDidChangeNotificationObserver {
            [weak self] note in
            guard let changeType = self?.changeType(of: object, in: note) else { return }
            handler(changeType)
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(token)
    }

    // MARK: Private

    private var token: NSObjectProtocol!

    private func changeType(
      of object: NSManagedObject,
      in note: ObjectDidChangeNotification)
      -> ChangeType?
    {
        let deleted = note.deletedObjects.union(note.invalidatedObjects)
        if note.invalidatedAllObjects || deleted.containsObjectIdentical(to: object) {
            return .delete
        }
        let updated = note.updatedObjects.union(note.refreshedObjects)
        if updated.containsObjectIdentical(to: object) {
            return .update
        }
        return nil
    }

}
