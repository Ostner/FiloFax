//
//  SegueHandler.swift
//  FiloFax
//
//  Created by Tobias Ostner on 4/19/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

protocol SegueHandler {
    associatedtype SegueIdentifier: RawRepresentable
}

extension SegueHandler where
  Self: UIViewController,
  SegueIdentifier.RawValue == String
{
    func segueIdentifier(for segue: UIStoryboardSegue) -> SegueIdentifier {
        guard let identifier = segue.identifier,
              let segueIdentifier = SegueIdentifier(rawValue: identifier)
        else { fatalError("Unknown segue: \(segue)") }
        return segueIdentifier
    }

}
