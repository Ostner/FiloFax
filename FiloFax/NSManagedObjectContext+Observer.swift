//
//  NSManagedObjectContext+Observer.swift
//  FiloFax
//
//  Created by Tobias Ostner on 4/23/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import CoreData

struct ObjectDidChangeNotification {

    init(note: Notification) {
        notification = note
    }

    var insertedObjects: Set<NSManagedObject> {
        return objects(forKey: NSInsertedObjectsKey)
    }

    var updatedObjects: Set<NSManagedObject> {
        return objects(forKey: NSUpdatedObjectsKey)
    }

    var deletedObjects: Set<NSManagedObject> {
        return objects(forKey: NSDeletedObjectsKey)
    }

    var refreshedObjects: Set<NSManagedObject> {
        return objects(forKey: NSRefreshedObjectsKey)
    }

    var invalidatedObjects: Set<NSManagedObject> {
        return objects(forKey: NSInvalidatedObjectsKey)
    }

    var invalidatedAllObjects: Bool {
        return (notification as Notification).userInfo?[NSInvalidatedAllObjectsKey] != nil
    }
    // MARK: Private

    private let notification: Notification

    private func objects(forKey key: String) -> Set<NSManagedObject> {
        return ((notification as Notification).userInfo?[key] as? Set<NSManagedObject>) ?? Set()
    }
}

extension NSManagedObjectContext {
    func addDidChangeNotificationObserver(_ handler: @escaping (ObjectDidChangeNotification) -> ()) -> NSObjectProtocol {
        let nc = NotificationCenter.default
        return nc.addObserver(
          forName: Notification.Name.NSManagedObjectContextObjectsDidChange,
          object: self,
          queue: nil) {
            note in
            let wrappedNote = ObjectDidChangeNotification(note: note)
            handler(wrappedNote)
        }
    }
}

extension Sequence where Iterator.Element: AnyObject {
    func containsObjectIdentical(to object: AnyObject) -> Bool {
        return contains { $0 === object }
    }
}
