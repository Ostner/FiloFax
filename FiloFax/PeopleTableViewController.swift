//
//  PeopleTableViewController.swift
//  FiloFax
//
//  Created by Tobias Ostner on 4/19/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreData

final class PeopleViewController: UITableViewController, SegueHandler {
    var managedContext: NSManagedObjectContext!

    enum SegueIdentifier: String {
        case personDetail = "PersonDetail"
        case newPerson = "NewPerson"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueIdentifier(for: segue) {
        case .personDetail:
            guard let destination = segue.destination as? PersonDetailViewController
            else { fatalError("Wrong view controller") }
            destination.person = dataSource.selectedObject
        case .newPerson:
            guard let destination = segue.destination as? NewPersonViewController
            else { fatalError("Wrong view controller") }
            destination.managedContext = managedContext
        }
    }

    // MARK: Private

    private var dataSource: TableViewDataSource<PeopleViewController>!

    private func setupTableView() {
        let request = Person.sortedFetchRequest
        let frc = NSFetchedResultsController(
          fetchRequest: request,
          managedObjectContext: managedContext,
          sectionNameKeyPath: nil,
          cacheName: nil)
        dataSource = TableViewDataSource(
          tableView: tableView,
          cellIdentifier: "PersonCell",
          fetchedResultsController: frc,
          delegate: self)
    }
}

extension PeopleViewController: TableViewDataSourceDelegate {
    func configure(_ cell: PersonCell, for object: Person) {
        cell.configure(for: object)
    }
}
