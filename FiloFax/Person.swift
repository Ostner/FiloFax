//
//  Person.swift
//  FiloFax
//
//  Created by Tobias Ostner on 4/19/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import CoreData
import UIKit

final class Person: NSManagedObject {
    @NSManaged fileprivate(set) var name: String
    @NSManaged fileprivate(set) var photo: Data?

    static func insert(
      into context: NSManagedObjectContext,
      name: String,
      image: UIImage? = nil)
      -> Person
    {
        let person: Person = context.insertObject()
        person.name = name
        if let image = image {
            let data = UIImagePNGRepresentation(image)
            person.photo = data
        }
        return person
    }

    var image: UIImage? {
        guard let data = photo else { return nil }
        return UIImage(data: data)
    }
}

extension Person: Managed {
    static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: #keyPath(name), ascending: false)]
    }
}
