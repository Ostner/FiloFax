//
//  PersonDetailViewController.swift
//  FiloFax
//
//  Created by Tobias Ostner on 4/19/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

final class PersonDetailViewController: UIViewController {

    var person: Person! {
        didSet {
            observer = ManagedObjectObserver(object: person) {
                [weak self] type in
                guard type == .delete else { return }
                _ = self?.navigationController?.popViewController(animated: true)
            }
        }
    }

    @IBOutlet var photo: UIImageView!
    @IBOutlet var name: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        let deleteBtn = UIBarButtonItem(
          barButtonSystemItem: .trash,
          target: self,
          action: #selector(deletePerson)
        )
        navigationItem.rightBarButtonItem = deleteBtn
        name.text = person.name
        photo.image = person.image
    }

    // MARK: Private

    private var observer: ManagedObjectObserver?

    // MARK: Action

    func deletePerson() {
        person.managedObjectContext?.performChanges {
            self.person.managedObjectContext?.delete(self.person)
        }
    }
}
