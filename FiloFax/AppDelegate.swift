//
//  AppDelegate.swift
//  FiloFax
//
//  Created by Tobias Ostner on 4/19/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var persistentContainer: NSPersistentContainer!

    func application(
      _ application: UIApplication,
      didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)
      -> Bool
    {
        createFiloFaxStack { container in
            self.persistentContainer = container
            let storyboard = self.window?.rootViewController?.storyboard
            guard let nvc = storyboard?.instantiateViewController(withIdentifier: "RootViewController") as? UINavigationController,
                  let vc = nvc.topViewController as? PeopleViewController
            else { fatalError("Wrong view controller type") }
            vc.managedContext = container.viewContext
            self.window?.rootViewController = nvc
        }
        return true
    }
}
