//
//  NSManagedObjectContext+Extensions.swift
//  FiloFax
//
//  Created by Tobias Ostner on 4/21/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import CoreData

extension NSManagedObjectContext {
    func insertObject<T: NSManagedObject>() -> T where T: Managed {
        guard let obj = NSEntityDescription
                .insertNewObject(forEntityName: T.entityName,
                                 into: self) as? T
        else { fatalError("Wrong object type") }
        return obj
    }

    func saveOrRollback() -> Bool {
        do {
            try save()
            return true
        } catch {
            rollback()
            return false
        }
    }

    func performChanges(block: @escaping () -> ()) {
        perform {
            block()
            _ = self.saveOrRollback()
        }
    }
}
